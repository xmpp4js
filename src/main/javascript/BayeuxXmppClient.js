Ext.namespace( "Xmpp4Js" );

/**
 * This is an experimental Bayeux connection class that is not currently used.
 * @constructor
 */
Xmpp4Js.BayeuxXmppClient = function() {
    this.addEvents({
        "beforesend" : true,
        "recv" : true
    });
    this.comet = dojox.cometd;
    
    this.comet.init( "/CometTest/cometd" );
    this.comet.subscribe( "/service/jabber", this.handlePacket.bind(this) );
}

Xmpp4Js.BayeuxXmppClient .prototype = {
    connect : function() {
        // no need to do anything yet
        //this.comet.publish( "/service/jabber", {packet : ""} );
    },
    handlePacket : function(msg) {
        var packetXml = msg.data.packet;

        var doc = new DOMParser().parseFromString(packetXml, 'text/xml');
        
        var stanza = this.stanzaProvider.fromNode( doc.documentElement );
        this.fireEvent( "recv", stanza );
    },
    send : function(stanza, callback) {
        this.fireEvent( "beforesend", stanza );
        var xml = serializeNode( stanza.getNode() );
        

        
        // register a temporary listener for the packet ID
        if( callback ) {
            var id = packet.getId();
            var filter = new Xmpp4Js.PacketFilter.PacketIdFilter( id );
            
            var wrappedListener;
            var wrappedCallback = function(packet) {
                this.removePacketListener( wrappedListener );
                callback(packet);
            }.bind(this);
            wrappedListener = this.addPacketListener( wrappedCallback, filter );
        }
        
        this.comet.publish( "/service/jabber", {packet : xml});
    },
    addPacketListener : function(listener, filter) {
        var wrappedListener = function(packet) {
            if( filter && filter.accept(packet) ) {
                listener(packet);
            }
        };
        this.on( "recv", wrappedListener, this);
        
        return wrappedListener;
    },
    removePacketListener : function( listener ) {
        this.un( "recv", listener)
    },
    
    
    
    authenticatePlaintext : function( username, password, resource ) {
        var iq = this.getPacketHelper().createAuthPlaintext( username, password, resource );
        iq.send( this );
    },
    
    getPacketHelper : function() {
        if( this.packetHelper == null ) {
            this.packetHelper = new Xmpp4Js.Packet.PacketHelper();
        }

        return this.packetHelper;
    }
    
}

Ext.extend(Xmpp4Js.BayeuxXmppClient , Ext.util.Observable, Xmpp4Js.BayeuxXmppClient .prototype);