// Copyright (C) 2007  Harlan Iverson <h.iverson at gmail.com>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


/**
* @class The main class in establishing a Jabber connection.
* @param {String} server The domain (and hostname) of the Jabber server to connect to.
* @param {String} baseUrl The URL to an XEP-0124 HTTP Binding proxy--bound by same origin policy.
* @constructor
* @deprecated used Xmpp4Js.Connection instead
*/
Xmpp4Js.JabberConnection = function(server, baseUrl) {
    /** @private @type String */
    this.baseUrl = baseUrl;
    
    /** TODO let domain and server be different, and specify port. @private @type String */
    this.domain = server;
    /** @private @type String */
    this.server = server;
    /** @private @type int */
    this.port = 5222;	

    /** @private @type boolean */
    this.connected = false;
    /** @private @type Xmpp4Js.IO.HttpBindingStream */
    this.stream = new Xmpp4Js.IO.HttpBindingStream( baseUrl );
    
    /** @private @type Xmpp4Js.Roster.PresenceManager */
    this.presenceManager = new Xmpp4Js.Roster.PresenceManager();
    
    this.addPacketListener( this._onTerminalError.bind(this), new Xmpp4Js.PacketFilter.TerminalErrorPacketFilter() );
    this.addPacketListener( this._onRecoverableError.bind(this), new Xmpp4Js.PacketFilter.RecoverableErrorPacketFilter() );
    this.addPacketListener( this.presenceManager.presencePacketListener.bind(this.presenceManager), new Xmpp4Js.PacketFilter.PacketTypeFilter(Xmpp4Js.Packet.Presence) );
    
    
    this._registerEvents();
}

Xmpp4Js.JabberConnection.prototype = {
    /**
     * @private
     */
    _registerEvents : function() {
        this.addEvents({
            /**
            * @event onconnect
            * Fires when connect has completed
            */
            "onconnect" : true,

            /**
            * @event onauth
            * Fires when auth has completed
            */
            "onauth" : true,

            /**
            * @event beginsession
            * Fired when beginsession is finished and authentication / registration 
            * / normal operation can happen.
            */
            "beginsession": true,

            /**
            * Fired when a terminal (non-recoverable) error is received from the server.
            *
            * @event terminate
            * @param connection {Xmpp4Js.JabberConnection}
            * @param title {String} the reason for termination
            * @param message {String} an explanation of the termination
            * @param bodyElement {Element} the body element, to access app-specific info.
            */
            "terminate" : true,

            /**
            * Fired when a recoverable error is received from the server.
            *
            * @event error
            * @param connection {Xmpp4Js.JabberConnection}
            * @param bodyElement {Element} the body element, to access app-specific info.
            */
            "error" : true,

            /**
            * Fired when the connection is closed by the client.
            *
            * @event error
            * @param connection {Xmpp4Js.JabberConnection}
            * @todo add unit test
            */
            "close" : true,
            
            "autherror" : true
        });
    },

    /**
    * Fire error event. 
    * @private
    */
    _onRecoverableError : function(bodyElement) {
        this.fireEvent( "error", this, bodyElement );
    },

    /**
    * Fire terminate event. Automatically invokes endSession.
    * @private
    */
    _onTerminalError : function(bodyElement) {
        var condition = bodyElement.getAttribute( "condition" );

        var title = Xmpp4Js.PacketFilter.TerminalErrorPacketFilter.conditions[ condition ].title
        var message = Xmpp4Js.PacketFilter.TerminalErrorPacketFilter.conditions[ condition ].message;

        this.fireEvent( "terminate", this, title, message, bodyElement );
        this.endSession();
    },

    /**
     * Adds a packet listener and returns its function. See PacketReader.
     * @return Function
     * @see Xmpp4Js.IO.PacketReader#addPacketListener
     */
    addPacketListener : function( listener, filter ) {
        return this.stream.getReader().addPacketListener( listener, filter );
    },

    /**
     * Removes a packet listener by function. See PacketReader.
     * @param {Function} listener The listener returned by addPacketListener
     * @see Xmpp4Js.IO.PacketReader#removePacketListener
     */
    removePacketListener : function( listener ) {
        return this.stream.getReader().removePacketListener( listener );
    },

    /**
    * Send a packet across the wire, and register a PacketIdListener if a callback
    * is supplied
    * 
    * @param {Xmpp4Js.Packet.Base} packet
    * @param {function} callback The callback to be invoked when the server sends a response with same ID.
    * @see Xmpp4Js.PacketFilter.PacketIdFilter
    */
    send : function( packet, callback ) {


        /*
        if( callback ) {
        var id = packet.getId();

        var pf = new Xmpp4Js.PacketFilter.PacketIdFilter(id);
        var listener = function(packet) { 
        callback(packet); 
        this.removePacketListener( listener );
        }.bind(this);

        this.addPacketListener( listener, pf );
        }
        */

        if( callback ) {
            var id = packet.getId();


            var pf = new Xmpp4Js.PacketFilter.PacketIdFilter( id );
            var listener = function( stanza ) {
                this.removePacketListener( listener );

                callback( stanza );
            }.bind(this);

            this.addPacketListener( listener, pf );	
        }


        this.stream.pw.write( packet );
    },

    /**
    * The same as send, except it takes an XML string instead of a Packet object.
    * 
    * @param {String} xml
    * @param {function} callback The callback to be invoked when the server sends a response with same ID.
    * @see #send
    */
    sendXml : function( xml, callback ) {
        // parse XML string into a packet
        var doc =  parseXmlToDoc(xml);

        var stanza = this.getStream().getReader().stanzaProvider.fromNode( doc.documentElement );

        return this.send( stanza, callback );
    },

    /**
    * Sends the initial request to the jabber server. This must be called before any
    * other requests can be sent. The 'beginsession' event is fired when packets 
    * may be sent.
    */
    beginSession : function() {
        // Only begin a session that is not connected
        if (!this.connected) {
            var writer = this.stream.getWriter();

            var packet = this.getPacketHelper().createPacket();

            packet.setAttribute( "wait", writer.wait );
            packet.setAttribute( "hold", writer.hold );
            packet.setAttribute( "to", this.domain );
            packet.setAttribute( "route", "xmpp:" + this.server + ":" + this.port);
            packet.setAttribute( "ver", "1.6");
            packet.setAttribute( "ack", "1");
            packet.setAttribute( "xml:lang", "en");
            packet.setAttribute( "xmlns:xmpp", "urn:xmpp:xbosh");
            packet.setAttribute( "xmpp:version", "1.0" );

            /*
            packet.appendChild( 
            Builder.node("features", {xmlns:"http://etherx.jabber.org/streams"}, [
            Builder.node("mechanisms", {xmlns: "urn:ietf:params:xml:ns:xmpp-sasl"}, [
            Builder.node( "mechanism", "DIGEST-MD5" ),
            Builder.node( "mechanism", "PLAIN" )
            ])
            ] ) 
            );
            */

            var listener = function( frame ) {
                // ONLY the packets we want contain a sid attribute
                if( !frame.hasAttribute( "sid" ) ) { 
                    console.error( "no sid on response" );
                    return;
                }

                this.removePacketListener( listener );

                var writer = this.stream.getWriter();

                // TODO refactor this stuff into HttpBindingStream.
                writer.sid = frame.getAttribute( "sid" );
                writer.maxRequests = frame.getAttribute( "requests" );
                writer.hold = writer.maxRequests - 1;
                writer.wait = frame.getAttribute("wait");

                this.connected = true;

                writer.setPolling( true );

                this.fireEvent( "beginsession", this );

                //this.stream.setAutoFlush(true);
            }.bind(this);

            var pf = new Xmpp4Js.PacketFilter.RawPacketFilter();

            this.addPacketListener( listener, pf );

            // write the inital request and stop sending packets until
            // we get a response.
            //this.stream.setAutoFlush(false);
            this.stream.getWriter().writeRaw( packet );
            //this.stream.getWriter().flush();

        }
    },

    /**
    * Sends a request to the server to pause for 120 seconds. I don't believe
    * this currently works.
    */
    pause : function() {
        var packet = this.getPacketHelper().createPacket();
        packet.setAttribute( "pause", 120 );
        this.stream.getWriter().writeRaw( packet );

        // TODO store stuff in cookie...
        // host,port,secure,_rid,_last_rid,_wait,_min_polling,_inactivity,_hold,_last_requests,_pause

    },

    /**
     * Resumes a paused connection. Not currently implemented.
     */
    resume : function() {

    },

    /**
    * Send the terminate request to end the session. Fires the "close" event.
    */
    endSession : function() {
        var writer = this.stream.getWriter();

        var packet = this.getPacketHelper().createPacket();

        packet.setAttribute( "type", "terminate" );
        packet.setAttribute( "wait", writer.wait );
        packet.setAttribute( "hold", writer.hold );
        packet.setAttribute( "to", this.domain );
        packet.setAttribute( "route", "xmpp:" + this.domain + ":"+this.port);
        packet.setAttribute( "ver", "1.6");
        packet.setAttribute( "ack", "1");
        packet.setAttribute( "xml:lang", "en");
        packet.setAttribute( "xmlns:xmpp", "urn:xmpp:xbosh");
        packet.setAttribute( "xmpp:version", "1.0" );

        this.stream.getWriter().writeRaw( packet );
        //this.stream.getWriter().flush();

        // FIXME for some reason terminate isn't sent if this is called... but it stops the flushing.
        this.stream.setAutoFlush(false);

        this.connected = false;

        this.fireEvent( "close", this );
    },

    /**
     * Returns the Jid of the currently connected user, if any.
     * @type Xmpp4Js.Jid
     */
    getJid : function() {
        return new Xmpp4Js.Jid(this.jid);	
    },

    /**
     * Authenticate as anonymous on the jabber server. Currently works by sending 
     * an empty auth packet which is supported on Openfire, but ideally it would
     * use SASL anonymous.
     */
    // TODO make this a packet type or something... that means it will need
    // a reference to connection in order to register its filter
    authenticateAnonymous : function() {
        var iq = this.getPacketHelper().createIQ( this.domain, "set", "jabber:iq:auth" );
        iq.setId( "auth1" );

        this.send( iq, function( responseIq ) {
            if( responseIq.getType() == 'error' ) {
                console.info( "error doing anonymous auth" );
            } else { 
            console.info( "anonymous auth success" );
            this.jid = responseIq.getTo();

            this.fireEvent( "onconnect" );
            this.fireEvent( "onauth" );
        }
    }.bind(this) );

    },

    /**
     * Send a 'plaintext' authentication request. The 'onauth' event is fired upon success. 
     * Packets not requiring an authenticated session may be exchanged before 'onauth' comes.
     * @param {String} username The username, without host part, to authenticate as.
     * @param {String} password The plaintext password
     * @param {String} resource The resource to use. 'xmpp4js' is the sensible default.
     */
    authenticatePlaintext : function( username, password, resource ) {
        if( !resource ) { resource = 'xmpp4js'; };
        var iq = this.getPacketHelper().createAuthPlaintext( username, password, resource );
        iq.send( this );
    },

    /**
     * Authenticates using SASL MD5. Currently flawed and hardly works. Fires the 'onauth' event on success.
     *
     * TODO clean up... messy as crap, but works.
     * get rid of sendXml stuff, add comments, etc. tired and its late.
     *
     * @param {String} username The username, without host part, to authenticate as.
     * @param {String} password The plaintext password
     * @param {String} resource The resource to use. 'xmpp4js' is the sensible default.
     *
     * @see #authenticatePlaintext
     */
    authenticateMd5 : function( username, password, resource ) {
        if( !resource ) { resource = 'xmpp4js'; };

        var sasl = new Md5Sasl( username, password, this.domain );

        var saslListener = function(bodyElement) {
            var elem = bodyElement.firstChild;

            if( elem.localName == "challenge" ) {
                var challenge = elem.textContent;
                var fields = sasl.deserializeFields(sasl.decodeChallenge(challenge));


                console.dir( fields );

                var response = "";
                if(fields.qop == "auth") {
                    response = sasl.computeChallengeResponse( challenge );

                    console.dir( sasl.deserializeFields(sasl.decodeChallenge(response)) );
                } else if( fields.rspauth ) {
                if( !sasl.checkResponse( challenge ) ) {
                    // problem...
                    throw new Error( "Invalid response from server!" );
                }
            }



            this.sendXml( '<response xmlns="urn:ietf:params:xml:ns:xmpp-sasl">'+response+'</response>' );
        } else if( elem.localName == "success" ) {
        // w00t.
        var writer = this.stream.getWriter();

        var packet = this.getPacketHelper().createPacket();
        packet.setAttribute( "xml:lang", "en");
        packet.setAttribute( "xmlns:xmpp", "urn:xmpp:xbosh");
        packet.setAttribute( "xmpp:restart", "true" );

        writer.writeRaw( packet );

        var bindIq = new Xmpp4Js.Packet.IQ( null, "set", "jabber:client" );
        bindIq.setId( "bind_1" );

        var doc = bindIq.getNode().ownerDocument;
        var bindNode = bindIq.getNode().appendChild( createElementNS(doc, "urn:ietf:params:xml:ns:xmpp-bind", "bind" ) );
        var resourceNode = bindNode.appendChild( createElementNS(doc, "urn:ietf:params:xml:ns:xmpp-bind", "resource" ) );
        resourceNode.textContent = resource;

        this.send( bindIq, function(respIq) {

            // FIXME bug that casuses infinite loop right here. For some reason
            //       this listener is being invoked for sess_1, which means
            //       that there's a bug in the packet reader stuff somewhere. aside
            //       from that, sasl auth works.


            var sessionIq = new Xmpp4Js.Packet.IQ( null, "set", "jabber:client" );
            sessionIq.setId( "sess_1" );

            var doc = sessionIq.getNode().ownerDocument;
            var sessionNode = sessionIq.getNode().appendChild( createElementNS(doc, "urn:ietf:params:xml:ns:xmpp-session", "session" ) );

            this.send( sessionIq, function(respIq) {
                this.jid = respIq.getTo();
                console.info( "authenticated as "+this.jid );

                this.fireEvent( "onconnect" );
                this.fireEvent( "onauth" );
            }.bind(this));

        }.bind(this));

        cleanup();
    } else if( elem.localName == "failure" ) {
    cleanup();
    this.close();
    }
    }.bind(this);

    var cleanup = function() {
        this.removePacketListener(saslListener);
    }.bind(this);

    this.addPacketListener( saslListener, new Xmpp4Js.PacketFilter.RawPacketFilter() );

    this.sendXml( '<auth xmlns="urn:ietf:params:xml:ns:xmpp-sasl" mechanism="DIGEST-MD5"/>' );

    /*

    var anonAuth = DomHelper.node( 
    "auth",
    { 
    xmlns: "urn:ietf:params:xml:ns:xmpp-sasl",
    mechanism: "ANONYMOUS"
    }
    );

    // http://tools.ietf.org/html/draft-saintandre-rfc3920bis-04
    var md5Auth = DomHelper.node( 
    "auth",
    { 
    xmlns: "urn:ietf:params:xml:ns:xmpp-sasl",
    mechanism: "DIGEST-MD5"
    }
    );

    var plainAuth = DomHelper.node( 
    "auth",
    { 
    xmlns: "urn:ietf:params:xml:ns:xmpp-sasl",
    mechanism: "PLAIN"
    }
    ,
    [
    username+"@"+this.domain+String.fromCharCode(0)+username+String.fromCharCode(0)+password
    ]
    );
    */
    },

    /**
     * Another implementation of authenticatePlaintext. Dunno.
     * @see #authenticatePlaintext
     */
    authenticatePlaintext2 : function( username, password, resource ) {
        var iq = this.getPacketHelper().createIQ( this.domain, "set", "jabber:iq:auth" );
        iq.setId( "auth1" );

        var query = iq.getQuery();
        var doc = query.ownerDocument;

        query.appendChild( doc.createElement( "username" ) ).textContent = username;
        query.appendChild( doc.createElement( "password" ) ).textContent = password;
        query.appendChild( doc.createElement( "resource" ) ).textContent = resource;
        query.appendChild( doc.createElement( "plaintext" ) );

        this.send( iq, function( responseIq ) {
            if( responseIq.getType() == 'error' ) {
                alert( "error doing plaintext auth" );
            } else { 
            alert( "plaintext auth success" );
            this.jid = responseIq.getTo();
        }
    } );
    },

    /** 
     * Register an account on the remote jabber server. This needs to be refactored out of here.
     */
    register : function( username, password, callback ) {
        var iq = this.getPacketHelper().createIQ( this.domain, "set", "jabber:iq:register" );
        iq.setId( "reg1" );

        var query = iq.getQuery();
        var doc = query.ownerDocument;

        query.appendChild( doc.createElement( "username" ) ).textContent = username;
        query.appendChild( doc.createElement( "password" ) ).textContent = password;

        this.send( iq, function( regIq ) {
            if( regIq.getType() == 'error' ) {
                alert( "error doing registration" );
            } 
            if(callback){ callback( regIq ); }
        } );
    },

    /** 
     * Unregister an account on the remote jabber server. This needs to be refactored out of here.
     */
    unregister : function() {
        var iq = this.getPacketHelper().createIQ( this.domain, "set", "jabber:iq:register" );
        iq.setId( "unreg1" );

        var query = iq.getQuery();
        var doc = query.ownerDocument;

        query.appendChild( doc.createElement( "remove" ) );

        this.send( iq, function( packet ) {
            if( iq.getType() == 'error' ) {
                alert( "error doing unregistration" );
            } 
        } );
    },

    /**
     * Gets the presence manager for the current connection.
     *
     * @return Xmpp4Js.Roster.PresenceManager
     */
    getPresenceManager : function() {
        return this.presenceManager
    },

    /**
     * No idea if this is even used.
     * @private
     */
    elemToXml : function( elem ) {
        var xml = Try.these(
            function() { return (new XMLSerializer()).serializeToString(elem); },
            function() { return (new XMLSerializer()).serializeToString(elem.ownerDocument); },
            function() { return elem.xml; }
        );

        return xml;
    },



    /**
     * @return Xmpp4Js.Packet.PacketHelper
     */
    getPacketHelper : function() {
        if( this.packetHelper == null ) {
            this.packetHelper = new Xmpp4Js.Packet.PacketHelper();
        }

        return this.packetHelper;
    },

    setPacketHelper : function(packetHelper) {
        this.packetHelper = packetHelper;
    },

    /**
     * @return Xmpp4Js.IO.HttpBindingStream
     */
    getStream : function() {
        return this.stream;
    }
}

Ext.extend(Xmpp4Js.JabberConnection, Ext.util.Observable, Xmpp4Js.JabberConnection.prototype);

/**
 * @constructor
 * @extends Xmpp4Js.JabberConnection
 * @deprecated Used by tests, but is worthless after refactoring
 */
Xmpp4Js.XmppDummyConnection = function(server, baseUrl) {
    Xmpp4Js.XmppDummyConnection.superclass.constructor.call(this, server, baseUrl);
}

Ext.extend(Xmpp4Js.XmppDummyConnection, Xmpp4Js.JabberConnection, {});


