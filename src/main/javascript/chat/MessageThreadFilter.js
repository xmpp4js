Ext.namespace( "Xmpp4Js.Chat" );

/**
* @class Filters for message packets with a particular thread value.
* @constructor
* @param {String} thread The thread ID to filter incoming Message packets for.
*/
Xmpp4Js.Chat.MessageThreadFilter = function(thread) {
    /** @private @type String */
    this.thread = thread;
    /** A filter for Message packets 
     * @private 
     * @type Xmpp4Js.PacketFilter.PacketTypeFilter 
     */
    this.packetTypeFilter = new Xmpp4Js.PacketFilter.PacketTypeFilter( Xmpp4Js.Packet.Message );
}

Xmpp4Js.Chat.MessageThreadFilter.prototype = {
    /**
     * Return true if this is a Message packet and its thread equals the one we're interested in.
     */
    accept: function(stanza) {
        return this.packetTypeFilter.accept(stanza)
        && stanza.getThread() == this.thread;
    }
}

Ext.extend( Xmpp4Js.Chat.MessageThreadFilter, Xmpp4Js.PacketFilter.PacketFilter, Xmpp4Js.Chat.MessageThreadFilter.prototype );