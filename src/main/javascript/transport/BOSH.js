Ext.namespace( "Xmpp4Js.Transport" );

/**
 * Functionality that needs testing:
 *  write:  
 *   sid 
 *     no sid on first request
 *    always present after session has started
 *   rid
 *    always present
 *    starts random
 *    sequential
 *    rollover at int limit
 *   key
 *    present / not present if it should be
 *    first, middle, last, first (init with length 3)
 *
 *  beginSession:
 *   rid, no sid, correct attributes
 *   error if called when open
 *   event is raised
 *
 *  endSession:
 *   terminate type and correct attributes are present
 *   error if called while not open
 *   event is raised
 *
 *  send:
 *   error before beginSession or after endSession
 *   multible nodes are combined to make one request
 *   number of open requests > max open requets
 *
 *  polling:
 *   doesn't send if there is an open request
 *   doesn't send if there are items in the queue
 *   sends empty body if both are empty
 */
Xmpp4Js.Transport.BOSH = function(config) {
    
    /**
     * The domain of the server you're connecting to.
     * @private
     */
    this.domain = config.domain;
    /**
     * The hostname or IP of the server to route to. defaults to domain.
     * @private
     */
    this.server = config.server || config.domain;
    /**
     * The port to route to. defaults to 5222
     * @private
     */
    this.port = config.port || 5222;
    /**
     * The time to wait for a response from the server, in seconds. defaults to 60 and can be adjusted by server.
     * @private
     */
    this.wait = config.wait || 60;
    
    /**
     * Picked up by Observable.
     * @private
     */
    this.listeners = config.listeners;
    
    
    /**
     * This is set to true when the session creation response is 
     * received and it was successful.
     *
     * @private
     */
    this.isSessionOpen = false;
    
    /**
     * @type Ext.util.TaskRunner
     * @private
     */
    this.taskRunner = new Ext.util.TaskRunner();
    
    /**
     * @private
     */
    this.sendQueueTask = {
        scope: this,
        run: this.sendQueue,
        interval: 500
    };
    
    /**
     * @private
     */
    this.sendPollTask = {
        scope: this,
        run: this.sendPoll,
        interval: 500
    };
    
    /**
     * @private
     */
    this.queue = [];
    
    /**
     * @private
     * @type String
     */
    this.endpoint = config.endpoint;
    
    /**
     * @type Ext.data.Connection
     * @private
     */
    this.xhr = this.createXhr();
    
    /** 
     * The number of open XHR requests. Used for polling.
     * @private
     */
    this.openRequestCount = 0;
    
    /**
     * The session ID sent by the server.
     * @private
     */
    this.sid = null;
    
    /**
     * The request ID set in beginSession, and cleared in endSession
     * @private
     */
    this.rid = null;
    
    /**
     * The keysequence object
     * @private
     */
    this.keySeq = null;
    
    /**
     * The max number of requests that can be open at once, sent by the server
     * @private
     */
    this.maxRequests = null;
    
    /**
     * The max number of requests that the server will keep open, sent by the server.
     * Typically this is maxRequests - 1.
     * @private
     */
    this.hold = null;
    
    var superConfig = Ext.apply( config, {
        
    });
    
    
    this.addEvents({
        /**
         * @event recv
         * @param {DomElement} the body element of the node received.
         *
         * A packet node has been received. Typically cline code will register
         * its recv handlers in response to the sessionStarted event and remove
         * them in response to the sessionEnded and termerror events.
         */
        recv : true,
        
        
        /**
         * @event write
         * @param {DomElement} the body element of the node about to be written.
         *
         * A packet node is about to be written. It includes all frame data, but 
         * the event is fired just before the open request count is incremented.
         */
        write : true,
        
        
        /**
         * @event error
         * @param {DomElement} the body element of the node received.
         *
         * A non-terminal error has occured. Connection is not necisarily closed.
         */
        error : true,
        
        /**
         * @event termerror
         * @param {String} title
         * @param {String} message
         * @param {DomElement} the body element of the node received.
         *
         * Raised when the session is been forcibly closed due to an error. 
         * Client code should remove any recv handlers here (should we remove?)
         */
        termerror : true,
        
        /**
         * @event sessionStarted
         *
         * Raised when the session has successfully be started. Clients should
         * register recv handlers here.
         */
        beginsession : true,
        
        /**
         * @event sessionEnded
         *
         * Raised when the session has been closed (voluntarily). Client code
         * should remove any recv handlers here (should we forcibly remove all?).
         */
        endsession: true
    });
    
    Xmpp4Js.Transport.BOSH.superclass.constructor.call( this, superConfig );

}

Xmpp4Js.Transport.BOSH.prototype = {
    /**
     * Create and return an instance of EXT's conneciton object
     * @return Ext.data.Connection
     * @private
     */
    createXhr: function() {
        return new Ext.data.Connection({
            url: this.endpoint,
            method: "POST",
            // FIXME this.wait is sent by initial response. Change this timeout
            //       when this.wait is changed. For now it's always at whatever
            //       this.wait is initialized to.
            timeout: this.wait * 1000,
            disableCaching: true,
            headers: { 'content-type': 'text/xml' }
        });
    },
    
    
    /**
     * Send a session creation request, and if it is successfully responded to
     * then mark the session open and start the sendQueueTask.
     */
    beginSession: function() {
        this.rid = this.createInitialRid();
        
        var packetNode = this.createPacketNode();
        packetNode.setAttribute( "wait", this.wait );
        packetNode.setAttribute( "to", this.domain );
        packetNode.setAttribute( "route", "xmpp:" + this.server + ":" + this.port);
        packetNode.setAttribute( "ver", "1.6");
        packetNode.setAttribute( "xml:lang", "en");
        packetNode.setAttribute( "xmlns:xmpp", "urn:xmpp:xbosh");
        packetNode.setAttribute( "xmpp:version", "1.0" );
        

        this.on("recv", this.onBeginSessionResponse, this, {single:true});
  
        this.write( packetNode );
    },
    
    /** 
     * Callback to the beginSession packet (recv event).
     *
     * @param {DomElement} packetNode
     * @private
     */
    onBeginSessionResponse: function(packetNode) {
        // HACK single doesn't seem to work...
        //this.un("recv", arguments.callee /* the current function */, this );


        this.sid = packetNode.getAttribute( "sid" ).toString();
        this.maxRequests = packetNode.getAttribute( "requests" ).toString();
        
        if( packetNode.hasAttribute("hold") ) {
            this.hold = packetNode.getAttribute("hold").toString();
        } else {
            // sensible default
            this.hold = packetNode.maxRequests - 1;
        }
        
        if( packetNode.hasAttribute("wait") ) {
            // FIXME ideally xhr's timeout should be updated
            this.wait = packetNode.getAttribute("wait").toString();
        }

        this.startup();

        this.fireEvent( "beginsession" );
    },
    
    /**
     * Set isSessionOpen to true and start sendQueue and sendPoll tasks
     * @private
     */
    startup: function() {
        this.isSessionOpen = true;
        this.taskRunner.start( this.sendQueueTask );
        this.taskRunner.start( this.sendPollTask );
        
    },
    
    /**
     * Send a terminate message, mark the sesion as closed, and stop the polling task.
     */
    endSession: function() {
        var packetNode = this.createPacketNode();
        packetNode.setAttribute( "type", "terminate" );
        
        // TODO we could be civil and append any remaining packets in the queue here.
        
        this.shutdown();

        this.write( packetNode );
        
        this.fireEvent( "endsession" );
    },
    
    /**
     * Set isSessionOpen to false and stop sendQueue and sendPoll tasks
     * @private
     */
    shutdown: function() {
        this.isSessionOpen = false;
        this.taskRunner.stop( this.sendQueueTask );
        this.taskRunner.stop( this.sendPollTask );
    },
    
    /**
     * Send a packet as soon as possible. If the session is not currently open,
     * packets will queue up until it is.
     * 
     * Should it throw an error if not currently open?
     *
     * @param {DomElement} node
     */
    send: function(node) {
        this.queue.push( node );
    },
    
    /**
     * Immediately write a raw packet node to the wire. Adds frame data including
     * RID, SID and Key if they are present.
     *
     * Also increments the openRequestCount, which is then decremented in the
     * onWriteResponse method.
     *
     * A possible addition could be to add a "no headers" flag.
     *
     * @param {DomElement} packetNode
     */
    write: function(packetNode) {
        
        this.addFrameData( packetNode );
        
        var xml = packetNode.toString();

        this.fireEvent( "write", packetNode );

        this.openRequestCount++;
        
        this.xhr.request({
            xmlData: xml,
            xmlNode: packetNode, // this isn't a real option... but it's needed for testing.
            scope: this,
            callback: this.onWriteResponse
        });
    },
    
    /**
     * Handles the response to a write call.
     *
     * Decrements the openRequestCount that was incremented in write.
     * @private
     */
    onWriteResponse: function(options, success, response) {
        this.openRequestCount--;
        
        var packetNode = null;
        
        if( response.responseText != null ) {
            // 17.4 XML Stanza Conditions?
            // this condition would be true if we closed the connection
            // before a response was received
            //
            // TODO setting xhr.timeout to a higher value than wait would
            //      eliminate this issue, unless there was a network 
            //      inturruption before the server responded. figure out
            //      how to handle this. 
            
            packetNode = new DOMImplementation().loadXML( response.responseText ).documentElement;
        }
        
        /*
            TODO - 17.1
            A legacy client (or connection manager) is a client (or 
            connection manager) that did not include a 'ver' attribute 
            in its session creation request (or response). A legacy 
            client (or connection manager) will interpret (or respond 
            with) HTTP error codes according to the table below. 
            Non-legacy connection managers SHOULD NOT send HTTP error 
            codes unless they are communicating with a legacy client. 
            Upon receiving an HTTP error (400, 403, 404), a legacy 
            client or any client that is communicating with a legacy 
            connection manager MUST consider the HTTP session to be 
            null and void. A non-legacy client that is communicating 
            with a non-legacy connection manager MAY consider that the 
            session is still active.
        */
        if( response.status == -1 ) {
            // we aborted the transaction, do nothing.
        } else if( !success || response.status != 200 ) {
            // 17.2 Terminal Binding Conditions
            this.shutdown();

            var condition = null;
            if( packetNode != null ) {
                condition = packetNode.getAttribute( "condition" ).toString();
            } else if( !response.status ) {
                condition = "undefined-condition";
            } else if( response.status != 200 ){
                condition = "status."+response.status;
            } else {
                condition = "undefined-condition";
            }
            
            var title = Xmpp4Js.PacketFilter.TerminalErrorPacketFilter.conditions[ condition ].title;
            var message = Xmpp4Js.PacketFilter.TerminalErrorPacketFilter.conditions[ condition ].message;

            this.fireEvent( "termerror", title, message, packetNode );
        } else if( packetNode.getAttribute("type").toString() == "error" ) {
            // 17.3 Recoverable Binding Conditions

            // TODO this should attempt to resend all packets back
            //      to the one that created the error. This could be
            //        implemented by putting each sent packet into a queue
            //        and removing it upon a successful response.
            //
            //        Ideally this error event would not even be visible beyond
            //        the the BOSH transport.
            
            this.fireEvent( "error", packetNode );
        } else {
            this.fireEvent( "recv", packetNode );
        }
    },
    
    /**
     * Create an empty packet node in the httpbind namespace.
     * @private
     * @return {DomElement} a body element with the correct namespace and basic attributes
     */ 
    createPacketNode: function() {
        var packetNode = DomBuilder.node( "body", {
                xmlns: "http://jabber.org/protocol/httpbind"
            }
        );
        
        return packetNode;
    },
    
    /**
     * Write a blank node if there is no data waiting and no requests open.
     * @private
     */
    sendPoll: function() {
        if( this.openRequestCount == 0 && this.queue.length == 0 ) {
            var packetNode = this.createPacketNode();
            this.write( packetNode );
        }
    },
    
    /**
     * Pull all packets off the queue; first-in, first-out; and send them
     * within the body of a single packet. Don't send if # open requests
     * is greater than max requests.
     *
     * @private
     */
    sendQueue: function() {
        // don't send anything if there is no work to do.
        if( this.queue.length == 0 || this.openRequestCount > this.maxRequests ) {
            return;
        }
        
        var packetNode = this.createPacketNode();

        while( this.queue.length > 0 ) {
            var node = this.queue.shift();
            var importedNode = packetNode.ownerDocument.importNode( node, true );

            packetNode.appendChild( importedNode );
        }
        
        this.write( packetNode );
    },

    /**
     * Add sid attribute to a packet, if there is one.
     * @param {Element} packetNode
     * @private
     */
    addSid: function( packetNode ) {
        if( this.sid !== null ) {
            packetNode.setAttribute( "sid", this.sid );
        }
    },
    
    /**
     * Add rid attribute to a packet, if there is one.
     * @param {Element} packetNode
     * @private
     */
    addRid: function( packetNode ) {   
        if( this.rid !== null ) {
            packetNode.setAttribute( "rid", this.rid++ );
        }
    },
    
    /**
     * Add the key attribute to the request, and if needed,
     * generate a new sequence and add the newkey attribute.
     * @param {Element} packetNode
     * @private
     */
    addKey: function( packetNode ) {
        if( this.keySeq instanceof KeySequence ) {
            var keySeq = this.keySeq;
            
            var isFirstKey = keySeq.isFirstKey();
            var isLastKey = keySeq.isLastKey();
            var key = keySeq.getNextKey();
            
            // if it's the first key, use ONLY the newkey attribute.
            if( isFirstKey ) {
                packetNode.setAttribute( "newkey", key );
            } else {
                packetNode.setAttribute( "key", key );
            }
            
            // if it's the last key, reset the KeySequence and add a newkey attribute.
            if( isLastKey ) {
;;;                console.info( "Resetting KeySequence" );
                keySeq.reset();
    
                var newKey = keySeq.getNextKey();
                packetNode.setAttribute( "newkey", newKey );
            }
        }
        
    },
    
    /**
     * Add RID, SID and Key to a packet node. Calls each respective function.
     * @private
     */
    addFrameData: function(packetNode) {
        this.addRid( packetNode );
        this.addSid( packetNode );
        this.addKey( packetNode );
    },
    
    /**
     * Generate a random number to be used as the initial request ID.
     * @private
     */
    createInitialRid: function() {
        return Math.floor( Math.random() * 10000 ); 
    }
}

Ext.extend( Xmpp4Js.Transport.BOSH, Ext.util.Observable, Xmpp4Js.Transport.BOSH.prototype );
