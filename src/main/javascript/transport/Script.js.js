Ext.namespace( "Xmpp4Js.Transport" );

/**
 * Functionality that needs testing:
 *  write:  
 *   sid 
 *     no sid on first request
 *    always present after session has started
 *   rid
 *    always present
 *    starts random
 *    sequential
 *    rollover at int limit
 *   key
 *    present / not present if it should be
 *    first, middle, last, first (init with length 3)
 *
 *  beginSession:
 *   rid, no sid, correct attributes
 *   error if called when open
 *   event is raised
 *
 *  endSession:
 *   terminate type and correct attributes are present
 *   error if called while not open
 *   event is raised
 *
 *  send:
 *   error before beginSession or after endSession
 *   multible nodes are combined to make one request
 *   number of open requests > max open requets
 *
 *  polling:
 *   doesn't send if there is an open request
 *   doesn't send if there are items in the queue
 *   sends empty body if both are empty
 */
Xmpp4Js.Transport.Script = function(config) {
    Xmpp4Js.Transport.Script.superclass.constructor.call( this, config );
}

Xmpp4Js.Transport.Script.prototype = {
    
    /**
     * Immediately write a raw packet node to the wire. Adds frame data including
     * RID, SID and Key if they are present.
     *
     * Also increments the openRequestCount, which is then decremented in the
     * onWriteResponse method.
     *
     * A possible addition could be to add a "no headers" flag.
     *
     * @param {DomElement} packetNode
     */
    write: function(packetNode) {
        
        this.addFrameData( packetNode );
        
        var xml = packetNode.toString();

        this.fireEvent( "write", packetNode );

        this.openRequestCount++;
        
        
        // TODO check for max length constraints in browsers
        var requestUrl = "http://"+this.server+":"+this.port+"/"+this.endpoint+"?body="+xml;
        var scriptElem = document.createElement( "script" );
        scriptElem.setAttribute( "type", "text/javascript" );
        scriptElem.setAttribute( "src", requestUrl );
        
        // TODO handle multiple connections...
        window._BOSH_ = function(xml) {
            this.handleResponse();
        }.bind(this);
        
        document.body.appendChild( scriptElem );
    },
    
    /**
     * Handles the response to a write call.
     *
     * Decrements the openRequestCount that was incremented in write.
     * @private
     */
    onWriteResponse: function( xml ) {
      this.openRequestCount--;

        // TODO character replacement (18.3)?
        
      var packetNode = new DOMImplementation().loadXML( xml ).documentElement;
      this.fireEvent( "recv", packetNode );
    }
}

Ext.extend( Xmpp4Js.Transport.Script, Xmpp4Js.Transport.BOSH, Xmpp4Js.Transport.Script.prototype );
