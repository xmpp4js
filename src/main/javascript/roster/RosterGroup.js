Ext.namespace( "Xmpp4Js.Roster" );

/** 
 *Protected constructor should only be called by Roster 
 * @constructor
 */
Xmpp4Js.Roster.RosterGroup = function(name, roster) {
      this.name = name;
      this.roster = roster;
    }
         
Xmpp4Js.Roster.RosterGroup.prototype = {
	getEntries: function() {
		var retEntries = [];
	
		$H(this.roster.map).each(function(pair) {
			var entry = pair.value;
			
			var groups = entry.groups;
			for( var j = 0; j < groups.length; j++ ) {
				var group = groups[j];
				if( group == this.name ) {
					retEntries.push( entry );
				}
			}
		}.bind(this));
		
		return retEntries;
	},
	getEntry: function(jid) {
		var entries = this.getEntries();
		var retEntry = undefined;
		$A(entries).each(function(entry) {
			if( entry.jid == jid ) {
				retEntry = entry;
			}
		}.bind(this));
		
		return retEntry;

	}
}