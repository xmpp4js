Ext.namespace( "Xmpp4Js.Roster" );

/**
 * @constructor
 * @extends Xmpp4Js.Roster.VirtualRosterGroup
 */
Xmpp4Js.Roster.UnfiledEntriesRosterGroup = function(roster) {
        Xmpp4Js.Roster.VirtualRosterGroup.superclass.constructor.call( this, "Unfiled Contacts", [], roster );	
}
         
Xmpp4Js.Roster.UnfiledEntriesRosterGroup.prototype = {
    getEntries: function() {
        var retEntries = [];

        $H(this.roster.map).each(function(pair) {
                var entry = pair.value;
                var groups = entry.groups;

                if( !groups || groups.length == 0 ) {
                        retEntries.push( entry );
                }
        }.bind(this));

        return retEntries;
    }
}
         
Ext.extend( Xmpp4Js.Roster.UnfiledEntriesRosterGroup, Xmpp4Js.Roster.VirtualRosterGroup, Xmpp4Js.Roster.UnfiledEntriesRosterGroup.prototype);
