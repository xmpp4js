// Copyright (C) 2007  Harlan Iverson <h.iverson at gmail.com>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * @constructor
 * @deprecated Ext.util.Observable is typically used.
 */
function DelegateManager() {
	this.map = {};
}

DelegateManager.prototype = {
	/**
	 * 
	 * @param {Function} func
	 * @return id to remove with
	 */
	add: function(func) {
		var id = "delegate_"+Math.random();
		this.map[id] = func;
		
		return id;
	
	},
	remove: function(id) {
		delete this.map[id];
	},
	/**
	 * Fire each delegate method with arguments that were passed to fire
	 */
	fire: function() {
		var fireArgs = arguments;
		$H(this.map).each(function(pair) {
			try {
				pair.value.apply(pair.value,fireArgs);
			} catch(e) {
				// TODO do something
			}
		});
	},
	// TODO test... even though it hardly needs it.
	getMap: function() {
		return this.map;
	}
}

/**
 * @constructor
 * @deprecated Ext.util.Observable is typically used.
 */
function EventListenerManager() {
	this.events = {};
	/** a map of event id => eventName */
	this.listenerEvents = {};
}

EventListenerManager.prototype.add = function(event, listener) {
	var dm = this.events[event];
	if( dm === undefined ) {
		this.events[event] = new DelegateManager();
		dm = this.events[event];
	}
	
	var id = dm.add( listener );
	this.listenerEvents[ id ] = event;
	
	return id;
}

EventListenerManager.prototype.remove = function(event, id) {
	// signature is changed to only take ID
	if( arguments.length == 1 ) {
		id = event;
		event = this.listenerEvents[ id ];
	}
	var dm = this.events[event];
	if( dm === undefined ) { return; }
	
	dm.remove( id );
}

EventListenerManager.prototype.getMap = function( event ) {
	var dm = this.events[event];
	if( dm === undefined ) { return; }
	
	return dm.getMap();
	
}

EventListenerManager.prototype.fireArgs = function( event, args ) {
	var callArgs = $A(args);
	// put event onto the beginning of the arg stack
	callArgs.unshift( event );
	
	this.fire.apply( this, callArgs );
}

EventListenerManager.prototype.fire = function( event ) {
	var dm = this.events[event];
	if( dm === undefined ) { return; }
	
	
	// get passed arguments and shift the first (event) off the front 
	var args = $A(arguments);
	args.shift();
	
	dm.fire.apply( dm, args );
}