Ext.namespace( "Xmpp4Js");

/**
 * <pre>var con = new Xmpp4Js.Connection({
 *     boshEndpoint: "/http-bind/"
 * });</pre>
 */
Xmpp4Js.Connection = function(config) {
    /** @private */
    this.transport = null;
    
    /**
     * Used for getInstanceFor as a substitute for hashCode, since I can't 
     * find one in JS.
     * @type String
     */
    this.id = Ext.id();
    
    this.stanzaProvider = config.stanzaProvider;
    
    this.transportConfig = config.transport;
    
    var superConfig = Ext.apply(config, {
    
    });
    
    this.addEvents({
        /**
         * The connection is open and ready for normal packets to be exchanged.
         */
        connect: true,
        /**
         * The connection was clsoed either forcibly or voluntarily. error
         * will be fired preceeding close if it was an forced termination.
         */
        close: true,
        /**
         * An error was received from the server. If the error is terminal,
         * the close event will be fired in succession
         *
         * TODO I don't like changing the order of the last three params
         *      from termerror, or that regular error doesn't have them. Then
         *      again, the goal is to not propagate recoverable error this far 
         *      anyway.
         * 
         * @param {Boolean} terminal
         * @param {DomElement} packetNode
         * @param {String} title
         * @param {String} message
         */
        error: true
    });
    
    /**
     * Picked up by Observable.
     * @private
     */
    this.listeners = config.listeners;
    
    this.domain = null;
    this.connected = false;
    this.packetHelper = new Xmpp4Js.Packet.PacketHelper();
    
    /** 
     * @private
     * @type Xmpp4Js.PacketListenerManager
     */
    this.packetListenerManager = new Xmpp4Js.PacketListenerManager({
        stanzaProvider: this.stanzaProvider
    });
    
    Xmpp4Js.Connection.superclass.constructor.call( this, superConfig );
    
}

Xmpp4Js.Connection.prototype = {
    /**
     * Connect to a given domain, port and server. Only domain is required.
     */
    connect: function(domain, port, server) {
        // TODO it would be nice to be able to give a connect
        //      event as a parameter, that registers on this and 
        //      unregisters right after it's called.
    
        if( this.isConnected() ) {
            throw new Xmpp4Js.Error( "Already Connected" );
        }
        this.domain = domain;

        var transportClass = this.transportConfig.clazz;
        
        if( typeof transportClass != 'function' ) {
            throw new Error( "transportClass is not valid." );
        }
        
        var transportConfig = Ext.apply( this.transportConfig, {
            domain: this.domain,
            port: port,
            server: server,
            listeners: {
                scope: this,
                termerror: this.onTerminalError,
                error: this.onError,
                beginsession: this.onBeginSession,
                endsession: this.onEndSession
                // recv will be added in beginSession.
            }
        });

        this.transport = new transportClass(transportConfig);
        this.transport.beginSession();
    },
    
    /**
     * Close the connection.
     */
    close: function() {
        if( !this.isConnected() ) {
            throw new Xmpp4Js.Error( "Not Connected" );
        }
        
        this.transport.endSession();
    },
    
    /**
     * Send a packet across the wire, and register a PacketIdListener if a callback
     * is supplied
     * 
     * @param {Xmpp4Js.Packet.Base} packet
     * @param {function} callback The callback to be invoked when the server sends a response with same ID.
     * @see Xmpp4Js.PacketFilter.PacketIdFilter
     */
    send: function(packet, callback) {
        if( !this.isConnected() ) {
            throw new Xmpp4Js.Error( "Not Connected" );
        }
        
        if( callback ) {
            var id = packet.getId();

            var pf = new Xmpp4Js.PacketFilter.PacketIdFilter( id );
            // TODO can this be abstracted to a OneTimePacketFilter that wraps
            //      any other PacketFilter?
            var listener = function( node ) {
                this.removePacketListener( listener );

                callback( node );
            }.bind(this);

            this.addPacketListener( listener, pf );    
        }
        
        this.transport.send( packet.getNode() );
    },
    
    /**
     * Returns whether or not er are connected.
     */
    isConnected: function() {
        return this.connected;
    },
    
    /**
     * Adds a packet listener and returns its function. See PacketReader.
     * @return Function
     * @see Xmpp4Js.PacketListenerManager#addPacketListener
     */
    addPacketListener : function( listener, filter ) {
        return this.packetListenerManager.addPacketListener( listener, filter );
    },

    /**
     * Removes a packet listener by function. See PacketReader.
     * @param {Function} listener The listener returned by addPacketListener
     * @see Xmpp4Js.PacketListenerManager#removePacketListener
     */
    removePacketListener : function( listener ) {
        return this.packetListenerManager.removePacketListener( listener );
    },
    
    /**
     * Called whenever an incoming packet comes. Handles dispatching
     * packet listeners / filters.
     *
     * @param {DomElement} packetNode
     * @private
     */
    onRecv: function(packetNode) {
        this.packetListenerManager.run( packetNode ); 
    },
    /**
     * Sets connected to false and removes the onRecv listener.
     * @private
     */
    onTerminalError: function(title, message, packetNode) {
        this.fireEvent( "error", true, packetNode, title, message );
        this.shutdown();
    },
    
    /**
     * Handle non-terminal errors
     * @param {DomElement} packetNode
     * @private
     */
    onError: function(packetNode) {
        this.fireEvent( "error", false, packetNode );
    },
    
    /**
     * Sets connected to true and sets up the onRecv listener.
     * @private
     */
    onBeginSession: function() {
        this.startup();
    },
    
    /**
     * Sets connected to false and removes the onRecv listener.
     * @private
     */
    onEndSession: function() {
        this.shutdown();
    },
    
    /**
     * Set connected to false, fire the close event, and remove the recv listener.
     * @private
     */
    shutdown: function() {
        this.transport.un("recv", this.onRecv, this );

        this.connected = false;
        this.fireEvent( "close" );
    },
       
    /**
     * Set connected to true, fire the connect event, and add the recv listener.
     * @private
     */
    startup: function() {
        this.connected = true;
        
        this.transport.on({
            scope: this,
            recv: this.onRecv
        });
        
        this.fireEvent( "connect" );
    },
    
    /**
     * @deprecated
     */
    getPacketHelper: function() {
        return this.packetHelper;
    },
    
    /**
     * Returns the Jid of the currently connected user, if any.
     * @type Xmpp4Js.Jid
     */
    getJid : function() {
        return new Xmpp4Js.Jid(this.jid);	
    }
}

Ext.extend( Xmpp4Js.Connection, Ext.util.Observable, Xmpp4Js.Connection.prototype );

