if( window['console'] == undefined ) {
    console = {
        info: Ext.emptyFn,
        log: Ext.emptyFn,
        dir: Ext.emptyFn,
        dirxml: Ext.emptyFn,
        warn: Ext.emptyFn,
        error: Ext.emptyFn
    }
}
